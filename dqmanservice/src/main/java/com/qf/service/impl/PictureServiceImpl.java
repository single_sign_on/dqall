package com.qf.service.impl;

import com.qf.dto.UploadDTO;
import com.qf.service.PictureService;
import com.qf.utils.FastDFSClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:41
 * @Author: yuemingyang
 * @Description:
 */
@Service
public class PictureServiceImpl implements PictureService {
    @Value("${pic_url}")
    private String pic_url;
    @Override
    public UploadDTO upload(MultipartFile multipartFile) {
        UploadDTO dto = new UploadDTO();
        if (multipartFile.isEmpty()) {
            dto.setError(1);
            dto.setMessage("上传失败");
            return dto;
        }
        String filename = multipartFile.getOriginalFilename();
        String substring = filename.substring(filename.lastIndexOf(".") + 1);
        try {
            FastDFSClient dfsClient = new FastDFSClient("properties/client.conf");
            String[] file = dfsClient.uploadFile(multipartFile.getBytes(), substring);
            String url = pic_url + file[0] + "/" + file[1];
            dto.setUrl(url);
            dto.setError(0);
            dto.setMessage("ok");
            return dto;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
