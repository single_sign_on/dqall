package com.qf.service.impl;

import com.qf.dao.DqAlbumsMapper;
import com.qf.dto.DqAblumsDTO;
import com.qf.pojo.DqAlbums;
import com.qf.service.DqAlbumsService;
import com.qf.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;

/**
 * @program: dqall
 * @Date: 2018/11/24 16:17
 * @Author: yuemingyang
 * @Description:
 */
@Service
public class DqAlbumsServiceImpl implements DqAlbumsService {
    @Resource
    private DqAlbumsMapper albumsMapper;
    @Override
    public void addAlbu(DqAblumsDTO dto) {
        DqAlbums pojo = new DqAlbums();
        BeanUtils.copyProperties(dto, pojo);
        try {
            pojo.setCreatedAt(DateUtils.stringToDate(dto.getCreatedAt()));
            pojo.setUpdatedAt(DateUtils.stringToDate(dto.getUpdatedAt()));
            albumsMapper.insert(pojo);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
