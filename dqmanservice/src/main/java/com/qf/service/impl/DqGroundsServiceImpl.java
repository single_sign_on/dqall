package com.qf.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.dao.DqGroundsMapper;
import com.qf.dto.DataGridDTO;
import com.qf.dto.DqGroundsDTO;
import com.qf.dto.ResultDTO;
import com.qf.pojo.DqGrounds;
import com.qf.service.DqGroundsService;
import com.qf.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

/**
 * @program: dqall
 * @Date: 2018/11/23 19:23
 * @Author: yuemingyang
 * @Description:
 */
@Service
public class DqGroundsServiceImpl implements DqGroundsService {
    @Autowired
    private DqGroundsMapper dqGroundsMapper;

    @Override
    public ResultDTO delete(Integer[] ids) {
        for (Integer id : ids) {
            dqGroundsMapper.deleteByPrimaryKey(id);
        }
        return ResultDTO.ok();
    }

    @Override
    public ResultDTO update(DqGroundsDTO dto) {
        DqGrounds pojo = new DqGrounds();
        BeanUtils.copyProperties(dto, pojo);
        try {
            pojo.setUpdatedAt(DateUtils.stringToDate(dto.getUpdatedAt()));
            dqGroundsMapper.updateByPrimaryKeySelective(pojo);
            return ResultDTO.ok();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int addGround(DqGroundsDTO groundsDTO) {
        DqGrounds grounds = new DqGrounds();
        BeanUtils.copyProperties(groundsDTO, grounds);
        try {
            grounds.setCreatedAt(DateUtils.stringToDate(groundsDTO.getCreatedAt()));
            grounds.setUpdatedAt(DateUtils.stringToDate(groundsDTO.getUpdatedAt()));
            dqGroundsMapper.insert(grounds);
            return grounds.getId();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataGridDTO getAll(int page, int rows) {
        PageHelper.startPage(page, rows);
        List<DqGrounds> dqGrounds = dqGroundsMapper.selectByExample(null);
        PageInfo info = new PageInfo(dqGrounds);
        DataGridDTO dto = new DataGridDTO();
        dto.setTotal(info.getTotal());
        dto.setRows(dqGrounds);
        return dto;
    }
}
