package com.qf.service.impl;

import com.qf.dao.DqEntitiesMapper;
import com.qf.dto.TreeDTO;
import com.qf.pojo.DqEntities;
import com.qf.service.DqEntitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:20
 * @Author: yuemingyang
 * @Description:
 */
@Service
public class DqEntitiesServiceImpl implements DqEntitiesService {
    @Autowired
    private DqEntitiesMapper entitiesMapper;
    @Override
    public List<TreeDTO> findAll() {
        List<TreeDTO> list = new ArrayList<>();
        List<DqEntities> entities = entitiesMapper.selectByExample(null);
        for (DqEntities entity : entities) {
            TreeDTO dto = new TreeDTO();
            dto.setId(entity.getOwnerId());
            dto.setText(entity.getOwnType());
            dto.setState("open");
            list.add(dto);
        }
        return list;
    }
}
