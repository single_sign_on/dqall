package com.qf.service.impl;

import com.qf.dao.DqAlbumsMapper;
import com.qf.dao.DqEntitiesMapper;
import com.qf.dao.DqGroundsMapper;
import com.qf.dto.PortalDTO;
import com.qf.pojo.*;
import com.qf.service.PortalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @program: dqall
 * @Date: 2018/11/26 11:14
 * @Author: yuemingyang
 * @Description:
 */
@Service
public class PortalServiceImpl implements PortalService {
    @Autowired
    private DqGroundsMapper groundsMapper;
    @Autowired
    private DqAlbumsMapper albumsMapper;
    @Autowired
    private DqEntitiesMapper entitiesMapper;
    @Override
    public PortalDTO getDTO(int groundId) {
        PortalDTO dto = new PortalDTO();
        dto.setId(groundId);
        DqGrounds grounds = groundsMapper.selectByPrimaryKey(groundId);
        dto.setName(grounds.getName());
        dto.setDescription(grounds.getDescription());
        Integer entryId = grounds.getDepotEntryId();
        DqEntitiesExample example = new DqEntitiesExample();
        DqEntitiesExample.Criteria criteria = example.createCriteria();
        criteria.andOwnerIdEqualTo(entryId);
        List<DqEntities> entities = entitiesMapper.selectByExample(example);
        dto.setOwnType(entities.get(0).getOwnType());
        DqAlbumsExample albumsExample = new DqAlbumsExample();
        DqAlbumsExample.Criteria criteria1 = albumsExample.createCriteria();
        criteria1.andSourceIdEqualTo(groundId);
        List<DqAlbums> dqAlbums = albumsMapper.selectByExample(albumsExample);
        String pictureUrl = dqAlbums.get(0).getPictureUrl();
        dto.setPictureUrl(pictureUrl);
        return dto;
    }
}
