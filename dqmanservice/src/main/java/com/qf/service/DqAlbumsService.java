package com.qf.service;

import com.qf.dto.DqAblumsDTO;

/**
 * @program: dqall
 * @Date: 2018/11/24 16:12
 * @Author: yuemingyang
 * @Description:
 */
public interface DqAlbumsService {
    public void addAlbu(DqAblumsDTO dto);
}
