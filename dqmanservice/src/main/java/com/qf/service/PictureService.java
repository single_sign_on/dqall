package com.qf.service;

import com.qf.dto.UploadDTO;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:37
 * @Author: yuemingyang
 * @Description:
 */
public interface PictureService {
    public UploadDTO upload(MultipartFile multipartFile);
}
