package com.qf.service;

import com.qf.dto.PortalDTO;

/**
 * @program: dqall
 * @Date: 2018/11/26 11:13
 * @Author: yuemingyang
 * @Description:
 */
public interface PortalService {
    public PortalDTO getDTO(int groundId);
}
