package com.qf.service;

import com.qf.dto.TreeDTO;

import java.util.List;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:19
 * @Author: yuemingyang
 * @Description:
 */
public interface DqEntitiesService {
    public List<TreeDTO> findAll();
}
