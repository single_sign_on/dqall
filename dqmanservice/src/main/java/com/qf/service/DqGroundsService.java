package com.qf.service;

import com.qf.dto.DataGridDTO;
import com.qf.dto.DqGroundsDTO;
import com.qf.dto.ResultDTO;

/**
 * @program: dqall
 * @Date: 2018/11/23 19:21
 * @Author: yuemingyang
 * @Description:
 */
public interface DqGroundsService {
    /**
     * 添加后返回主键
     * @param groundsDTO
     * @return
     */
    public int addGround(DqGroundsDTO groundsDTO);
    public DataGridDTO getAll(int page, int rows);
    public ResultDTO update(DqGroundsDTO dto);
    public ResultDTO delete(Integer[] ids);
}
