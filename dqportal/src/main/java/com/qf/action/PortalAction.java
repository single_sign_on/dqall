package com.qf.action;

import com.qf.dto.PortalDTO;
import com.qf.utils.HttpClientUtil;
import com.qf.utils.JsonUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @program: dqall
 * @Date: 2018/11/26 17:28
 * @Author: yuemingyang
 * @Description:
 */
@Controller
public class PortalAction {
    @RequestMapping("/index.html")
    public ModelAndView show(ModelAndView modelAndView) {
        String doGet = HttpClientUtil.doGet("http://localhost:8080/rest/show");
        PortalDTO dto = JsonUtils.jsonToPojo(doGet, PortalDTO.class);
        modelAndView.addObject("ptDto", dto);
        modelAndView.setViewName("index.jsp");
        return modelAndView;
    }
}
