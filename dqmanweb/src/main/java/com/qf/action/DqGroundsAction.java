package com.qf.action;

import com.github.pagehelper.util.StringUtil;
import com.qf.dto.DataGridDTO;
import com.qf.dto.DqAblumsDTO;
import com.qf.dto.DqGroundsDTO;
import com.qf.dto.ResultDTO;
import com.qf.service.DqAlbumsService;
import com.qf.service.DqGroundsService;
import com.qf.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @program: dqall
 * @Date: 2018/11/24 16:24
 * @Author: yuemingyang
 * @Description:
 */
@Controller
public class DqGroundsAction {
    @Autowired
    private DqGroundsService groundsService;
    @Autowired
    private DqAlbumsService albumsService;
    @RequestMapping("/item/groundsave")
    @ResponseBody
    public ResultDTO add(HttpServletRequest request) {
        String entryid = request.getParameter("entryid");
        String name = request.getParameter("name");
        String image = request.getParameter("image");
        String description = request.getParameter("description");
        DqGroundsDTO dto = new DqGroundsDTO();
        Date date = new Date();
        dto.setCreatedAt(DateUtils.dateToString(date));
        dto.setUpdatedAt(DateUtils.dateToString(date));
        dto.setDescription(description);
        dto.setDepotEntryId(Integer.parseInt(entryid));
        dto.setName(name);
        int ground = groundsService.addGround(dto);
        if (!StringUtil.isEmpty(String.valueOf(image))) {
            DqAblumsDTO ablumsDTO = new DqAblumsDTO();
            ablumsDTO.setCreatedAt(DateUtils.dateToString(date));
            ablumsDTO.setUpdatedAt(DateUtils.dateToString(date));
            ablumsDTO.setDescription(description);
            ablumsDTO.setPictureUrl(image);
            ablumsDTO.setSourceId(ground);
            ablumsDTO.setName(name);
            albumsService.addAlbu(ablumsDTO);
            return ResultDTO.ok();
        }
        return null;
    }
    @RequestMapping("/item/groundlist")
    @ResponseBody
    public DataGridDTO getList(int page, int rows) {
        return groundsService.getAll(page, rows);
    }
    @RequestMapping("/item/groundedit")
    @ResponseBody
    public ResultDTO update(HttpServletRequest request) {
        String id = request.getParameter("id");
        String entryid = request.getParameter("entryid");
        String name = request.getParameter("name");
        DqGroundsDTO dto = new DqGroundsDTO();
        dto.setName(name);
        dto.setUpdatedAt(DateUtils.dateToString(new Date()));
        dto.setId(Integer.parseInt(id));
        return groundsService.update(dto);
    }
    @RequestMapping("/item/grounddelete")
    @ResponseBody
    public ResultDTO delete(Integer[] ids) {
        return groundsService.delete(ids);
    }
}
