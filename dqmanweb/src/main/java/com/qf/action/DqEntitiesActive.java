package com.qf.action;

import com.qf.dto.TreeDTO;
import com.qf.service.DqEntitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:24
 * @Author: yuemingyang
 * @Description:
 */
@Controller
public class DqEntitiesActive {
    @Autowired
    private DqEntitiesService entitiesService;
    @RequestMapping("/item/entrylist")
    @ResponseBody
    public List<TreeDTO> getTree() {
        return entitiesService.findAll();
    }
}
