package com.qf.action;

import com.qf.dto.UploadDTO;
import com.qf.service.PictureService;
import com.qf.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:53
 * @Author: yuemingyang
 * @Description:
 */
@Controller
public class PictureActive {
    @Autowired
    private PictureService pictureService;
    @RequestMapping(value = "/pic/upload", produces = "text/html;charset=utf-8")
    @ResponseBody
    public String upload(@RequestParam("uploadFile") MultipartFile multipartFile) {
        UploadDTO upload = pictureService.upload(multipartFile);
        String toJson = JsonUtils.objectToJson(upload);
        return toJson;
    }
}
