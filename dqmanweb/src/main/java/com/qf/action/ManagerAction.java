package com.qf.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:07
 * @Author: yuemingyang
 * @Description:
 */
@Controller
public class ManagerAction {
    @RequestMapping("/{page}")
    public String goToPage(@PathVariable("page") String page) {
        return page;
    }
}
