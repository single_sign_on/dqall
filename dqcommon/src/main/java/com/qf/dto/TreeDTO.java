package com.qf.dto;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:16
 * @Author: yuemingyang
 * @Description:
 */
public class TreeDTO {
    private Integer id;
    private String text;
    private String state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
