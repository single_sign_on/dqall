package com.qf.dto;

import java.util.List;

/**
 * @program: dqall
 * @Date: 2018/11/24 16:52
 * @Author: yuemingyang
 * @Description:
 */
public class DataGridDTO {
    private long total;
    private List rows;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }
}
