package com.qf.dto;

/**
 * @program: dqall
 * @Date: 2018/11/23 19:16
 * @Author: yuemingyang
 * @Description:
 */
public class DqGroundsDTO {
    private Integer id;

    private Integer depotEntryId;

    private String name;

    private String createdAt;

    private String updatedAt;

    private String description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDepotEntryId() {
        return depotEntryId;
    }

    public void setDepotEntryId(Integer depotEntryId) {
        this.depotEntryId = depotEntryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
