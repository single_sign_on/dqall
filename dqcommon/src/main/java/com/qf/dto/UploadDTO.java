package com.qf.dto;

/**
 * @program: dqall
 * @Date: 2018/11/24 11:34
 * @Author: yuemingyang
 * @Description:
 */
public class UploadDTO {
    private String url;
    private int error;
    private String message;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
