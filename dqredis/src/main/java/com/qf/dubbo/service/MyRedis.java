package com.qf.dubbo.service;

/**
 * @program: dqall
 * @Date: 2018/11/26 18:54
 * @Author: yuemingyang
 * @Description:
 */
public interface MyRedis {
    public void set(String key, String value);
    public String get(String key);
    public Long del(String key);
    public Long incr(String key);
    public Long desc(String key);
    public Long expire(String key, int timeout);
}
