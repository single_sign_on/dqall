package com.qf.dao;

import com.qf.pojo.DqUserCreditProfiles;
import com.qf.pojo.DqUserCreditProfilesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqUserCreditProfilesMapper {
    long countByExample(DqUserCreditProfilesExample example);

    int deleteByExample(DqUserCreditProfilesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DqUserCreditProfiles record);

    int insertSelective(DqUserCreditProfiles record);

    List<DqUserCreditProfiles> selectByExample(DqUserCreditProfilesExample example);

    DqUserCreditProfiles selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DqUserCreditProfiles record, @Param("example") DqUserCreditProfilesExample example);

    int updateByExample(@Param("record") DqUserCreditProfiles record, @Param("example") DqUserCreditProfilesExample example);

    int updateByPrimaryKeySelective(DqUserCreditProfiles record);

    int updateByPrimaryKey(DqUserCreditProfiles record);
}