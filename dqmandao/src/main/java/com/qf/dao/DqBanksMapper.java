package com.qf.dao;

import com.qf.pojo.DqBanks;
import com.qf.pojo.DqBanksExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqBanksMapper {
    long countByExample(DqBanksExample example);

    int deleteByExample(DqBanksExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DqBanks record);

    int insertSelective(DqBanks record);

    List<DqBanks> selectByExample(DqBanksExample example);

    DqBanks selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DqBanks record, @Param("example") DqBanksExample example);

    int updateByExample(@Param("record") DqBanks record, @Param("example") DqBanksExample example);

    int updateByPrimaryKeySelective(DqBanks record);

    int updateByPrimaryKey(DqBanks record);
}