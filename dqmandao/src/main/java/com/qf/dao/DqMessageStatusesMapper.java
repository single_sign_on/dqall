package com.qf.dao;

import com.qf.pojo.DqMessageStatuses;
import com.qf.pojo.DqMessageStatusesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqMessageStatusesMapper {
    long countByExample(DqMessageStatusesExample example);

    int deleteByExample(DqMessageStatusesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DqMessageStatuses record);

    int insertSelective(DqMessageStatuses record);

    List<DqMessageStatuses> selectByExample(DqMessageStatusesExample example);

    DqMessageStatuses selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DqMessageStatuses record, @Param("example") DqMessageStatusesExample example);

    int updateByExample(@Param("record") DqMessageStatuses record, @Param("example") DqMessageStatusesExample example);

    int updateByPrimaryKeySelective(DqMessageStatuses record);

    int updateByPrimaryKey(DqMessageStatuses record);
}