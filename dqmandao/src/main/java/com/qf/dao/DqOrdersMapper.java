package com.qf.dao;

import com.qf.pojo.DqOrders;
import com.qf.pojo.DqOrdersExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqOrdersMapper {
    long countByExample(DqOrdersExample example);

    int deleteByExample(DqOrdersExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DqOrders record);

    int insertSelective(DqOrders record);

    List<DqOrders> selectByExampleWithBLOBs(DqOrdersExample example);

    List<DqOrders> selectByExample(DqOrdersExample example);

    DqOrders selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DqOrders record, @Param("example") DqOrdersExample example);

    int updateByExampleWithBLOBs(@Param("record") DqOrders record, @Param("example") DqOrdersExample example);

    int updateByExample(@Param("record") DqOrders record, @Param("example") DqOrdersExample example);

    int updateByPrimaryKeySelective(DqOrders record);

    int updateByPrimaryKeyWithBLOBs(DqOrders record);

    int updateByPrimaryKey(DqOrders record);
}