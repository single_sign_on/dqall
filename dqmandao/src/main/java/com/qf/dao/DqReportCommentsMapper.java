package com.qf.dao;

import com.qf.pojo.DqReportComments;
import com.qf.pojo.DqReportCommentsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqReportCommentsMapper {
    long countByExample(DqReportCommentsExample example);

    int deleteByExample(DqReportCommentsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DqReportComments record);

    int insertSelective(DqReportComments record);

    List<DqReportComments> selectByExample(DqReportCommentsExample example);

    DqReportComments selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DqReportComments record, @Param("example") DqReportCommentsExample example);

    int updateByExample(@Param("record") DqReportComments record, @Param("example") DqReportCommentsExample example);

    int updateByPrimaryKeySelective(DqReportComments record);

    int updateByPrimaryKey(DqReportComments record);
}