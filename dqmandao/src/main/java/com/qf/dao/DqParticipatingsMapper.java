package com.qf.dao;

import com.qf.pojo.DqParticipatings;
import com.qf.pojo.DqParticipatingsExample;
import com.qf.pojo.DqParticipatingsWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqParticipatingsMapper {
    long countByExample(DqParticipatingsExample example);

    int deleteByExample(DqParticipatingsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DqParticipatingsWithBLOBs record);

    int insertSelective(DqParticipatingsWithBLOBs record);

    List<DqParticipatingsWithBLOBs> selectByExampleWithBLOBs(DqParticipatingsExample example);

    List<DqParticipatings> selectByExample(DqParticipatingsExample example);

    DqParticipatingsWithBLOBs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DqParticipatingsWithBLOBs record, @Param("example") DqParticipatingsExample example);

    int updateByExampleWithBLOBs(@Param("record") DqParticipatingsWithBLOBs record, @Param("example") DqParticipatingsExample example);

    int updateByExample(@Param("record") DqParticipatings record, @Param("example") DqParticipatingsExample example);

    int updateByPrimaryKeySelective(DqParticipatingsWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(DqParticipatingsWithBLOBs record);

    int updateByPrimaryKey(DqParticipatings record);
}