package com.qf.dao;

import com.qf.pojo.DqLeveLset;
import com.qf.pojo.DqLeveLsetExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqLeveLsetMapper {
    long countByExample(DqLeveLsetExample example);

    int deleteByExample(DqLeveLsetExample example);

    int deleteByPrimaryKey(Integer setid);

    int insert(DqLeveLset record);

    int insertSelective(DqLeveLset record);

    List<DqLeveLset> selectByExample(DqLeveLsetExample example);

    DqLeveLset selectByPrimaryKey(Integer setid);

    int updateByExampleSelective(@Param("record") DqLeveLset record, @Param("example") DqLeveLsetExample example);

    int updateByExample(@Param("record") DqLeveLset record, @Param("example") DqLeveLsetExample example);

    int updateByPrimaryKeySelective(DqLeveLset record);

    int updateByPrimaryKey(DqLeveLset record);
}