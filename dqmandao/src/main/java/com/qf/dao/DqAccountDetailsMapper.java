package com.qf.dao;

import com.qf.pojo.DqAccountDetails;
import com.qf.pojo.DqAccountDetailsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqAccountDetailsMapper {
    long countByExample(DqAccountDetailsExample example);

    int deleteByExample(DqAccountDetailsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DqAccountDetails record);

    int insertSelective(DqAccountDetails record);

    List<DqAccountDetails> selectByExample(DqAccountDetailsExample example);

    DqAccountDetails selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DqAccountDetails record, @Param("example") DqAccountDetailsExample example);

    int updateByExample(@Param("record") DqAccountDetails record, @Param("example") DqAccountDetailsExample example);

    int updateByPrimaryKeySelective(DqAccountDetails record);

    int updateByPrimaryKey(DqAccountDetails record);
}