package com.qf.dao;

import com.qf.pojo.DqClubs;
import com.qf.pojo.DqClubsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DqClubsMapper {
    long countByExample(DqClubsExample example);

    int deleteByExample(DqClubsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DqClubs record);

    int insertSelective(DqClubs record);

    List<DqClubs> selectByExampleWithBLOBs(DqClubsExample example);

    List<DqClubs> selectByExample(DqClubsExample example);

    DqClubs selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") DqClubs record, @Param("example") DqClubsExample example);

    int updateByExampleWithBLOBs(@Param("record") DqClubs record, @Param("example") DqClubsExample example);

    int updateByExample(@Param("record") DqClubs record, @Param("example") DqClubsExample example);

    int updateByPrimaryKeySelective(DqClubs record);

    int updateByPrimaryKeyWithBLOBs(DqClubs record);

    int updateByPrimaryKey(DqClubs record);
}