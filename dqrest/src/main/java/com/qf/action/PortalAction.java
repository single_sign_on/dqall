package com.qf.action;

import com.github.pagehelper.util.StringUtil;
import com.qf.dto.PortalDTO;
import com.qf.dubbo.service.MyRedis;
import com.qf.service.PortalService;
import com.qf.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: dqall
 * @Date: 2018/11/26 11:12
 * @Author: yuemingyang
 * @Description:
 */
@RestController
public class PortalAction {
    @Autowired
    private PortalService portalService;
    @Autowired
    private MyRedis myRedis;
    @RequestMapping("/rest/show")
    public PortalDTO show() {
        try {
            String dq_portal = myRedis.get("dq_portal");
            if (! StringUtil.isEmpty(dq_portal)) {
                PortalDTO pojo = JsonUtils.jsonToPojo(dq_portal, PortalDTO.class);
                return pojo;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        PortalDTO dto = portalService.getDTO(14);
        try {
            String toJson = JsonUtils.objectToJson(dto);
            myRedis.set("dq_portal", toJson);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dto;
    }
    @RequestMapping("/rest/test")
    public Map<String, Object> test(String name, String addr) {
        Map<String, Object> map = new ConcurrentHashMap<>();
        map.put("name", name);
        map.put("addr", addr);
        return map;
    }
}
