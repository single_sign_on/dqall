package com.qf.action;

import com.qf.dubbo.service.MyRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @program: dqall
 * @Date: 2018/11/26 19:41
 * @Author: yuemingyang
 * @Description:
 */
@Controller
public class RedisUpdate {
    @Autowired
    private MyRedis redisService;
    @RequestMapping("/redis/del/{key}")
    @ResponseBody
    public Map<String, Object> del(@PathVariable String key) {
        Map<String, Object> map = new ConcurrentHashMap<>();
        Long del = redisService.del(key);
        if (del > 0) {
            map.put("code", 0);
            map.put("msg", "删除成功");
        } else {
            map.put("code", -1);
            map.put("msg", "删除失败");
        }
        return map;
    }
}
