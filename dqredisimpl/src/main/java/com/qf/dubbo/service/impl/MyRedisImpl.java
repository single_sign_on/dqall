package com.qf.dubbo.service.impl;

import com.qf.dubbo.service.MyRedis;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @program: dqall
 * @Date: 2018/11/26 19:08
 * @Author: yuemingyang
 * @Description:
 */
public class MyRedisImpl implements MyRedis {
    private JedisPool jedisPool;

    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    @Override
    public void set(String key, String value) {
        Jedis jedis = jedisPool.getResource();
        jedis.set(key, value);
    }

    @Override
    public String get(String key) {
        Jedis jedis = jedisPool.getResource();
        String s = jedis.get(key);
        return s;
    }

    @Override
    public Long del(String key) {
        Jedis jedis = jedisPool.getResource();
        Long del = jedis.del(key);
        return del;
    }

    @Override
    public Long incr(String key) {
        Jedis jedis = jedisPool.getResource();
        Long incr = jedis.incr(key);
        return incr;
    }

    @Override
    public Long desc(String key) {
        Jedis jedis = jedisPool.getResource();
        Long decr = jedis.decr(key);
        return decr;
    }

    @Override
    public Long expire(String key, int timeout) {
        Jedis jedis = jedisPool.getResource();
        Long expire = jedis.expire(key, timeout);
        return expire;
    }
}
